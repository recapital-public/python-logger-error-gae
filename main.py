# Copyright 2015 Google Inc. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# [START gae_flex_quickstart]
import json
import logging
import os
import sys
import time
import queue
import random
from collections import deque
from datetime import datetime
from threading import Thread

from flask import Flask, jsonify
from multiprocessing import Event, Process
from google.cloud.logging import Client

# variables that are accessible from anywhere
aliveQueue = deque(maxlen=1)
# lock to control access to variable
shouldRun = Event()
# ThreadExample
te = None
# ProcessExample
pe = None
# Timestamp
timestamp = None


class StackdriverFormatter(logging.Formatter):

    def __init__(self, *args, **kwargs):
        super(StackdriverFormatter, self).__init__(*args, **kwargs)

    def format(self, record):
        return json.dumps({
            'severity': record.levelname,
            'message': record.getMessage(),
            'name': record.name,
            'funcName': record.funcName
        })


if os.getenv("PRODUCTION", default='false').lower() in ('true', '1', 't'):
    # Instantiates a client
    client = Client()

    # Retrieves a Cloud Logging handler based on the environment
    # you're running in and integrates the handler with the
    # Python logging module. By default this captures all logs
    # at INFO level and higher
    client.get_default_handler()
    client.setup_logging(log_level=logging.INFO)
    logging.StreamHandler().setFormatter(StackdriverFormatter())
else:
    logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)

t_logger = logging.getLogger('ThreadExample')


class ThreadExample(Thread):

    def __init__(self, aliveQueue, shouldRun, callback):
        Thread.__init__(self)
        self.q = queue.Queue()
        self.aliveQueue = aliveQueue
        self.shouldRun = shouldRun
        self.callback = callback

    def load(self):
        t_logger.info("I am alive")

    def dequeue(self):
        while self.shouldRun:
            item = self.q.get()
            t_logger.info(f"Pulling {item} from queue!")
        t_logger.info("Terminating dequeue")

    def item(self):
        return self.item()

    def enqueue(self):
        while self.shouldRun.is_set():
            input = random.randint(1, 500)
            t_logger.info(f"Putting {input} in queue, current alive: {self.shouldRun.is_set()}.")
            self.q.put(input)
            time.sleep(5.0)
            self.aliveQueue.append(datetime.now())
        t_logger.info("Terminating enqueue")
        self.callback()

    def run(self):
        thread = Thread(target=self.dequeue)
        thread.start()
        self.enqueue()
        self.join()
        thread.join()


p_logger = logging.getLogger('ProcessExample')


class ProcessExample(Process):

    def __init__(self):
        super().__init__()

    def run(self):
        while True:
            p_logger.info("Executing run method in Process!!")
            time.sleep(4)


logger = logging.getLogger('Main')


def terminated():
    global te, shouldRun, aliveQueue
    logger.info("Calling terminated from outside.")
    if te is not None:
        te.join()
    if pe is not None:
        pe.join()
    shouldRun = True
    te = ThreadExample(aliveQueue, shouldRun, terminated)


def create_app():
    new_app = Flask(__name__)

    def setup():
        global config, te, pe
        shouldRun.set()
        te = ThreadExample(aliveQueue, shouldRun, terminated)
        te.start()
        pe = ProcessExample()

    setup()

    return new_app


app = create_app()


@app.route('/')
def hello():
    """Return a friendly HTTP greeting."""
    logging.info(f"test_1234 is here!")
    return jsonify(config)


@app.route('/alive')
def alive():
    global timestamp, shouldRun
    if 0 == len(aliveQueue):
        return "Not alive", 404
    elif timestamp is None:
        timestamp = aliveQueue.pop()
        return "Alive, first", 200
    else:
        new_timestamp = aliveQueue.pop()
        if new_timestamp is timestamp:
            return "Not alive, same timestamp", 404
        else:
            timestamp = new_timestamp
            return "Alive", 200


@app.route('/ready')
def ready():
    return 'ready', 200


@app.errorhandler(500)
def server_error(e):
    logging.exception('An error occurred during a request.')
    return """
    An internal error occurred: <pre>{}</pre>
    See logs for full stacktrace.
    """.format(e), 500


if __name__ == '__main__':
    # This is used when running locally. Gunicorn is used to run the
    # application on Google App Engine. See entrypoint in app.yaml.
    logger.info("Executing Sleep")
    time.sleep(2)
    logger.info("Starting Process")
    pe.start()
    app.run(host='127.0.0.1', port=8080, debug=True)

# [END gae_flex_quickstart]
